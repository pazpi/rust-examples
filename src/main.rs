use std::error;
use std::fs;
use std::io;
use std::path;
use std::result::Result::Err;

fn show_valid_rust_file(root_dir: &fs::DirEntry) -> Result<(), Box<dyn error::Error>> {
    let direntry = match get_list_entry(root_dir) {
        Ok(direntry) => direntry,
        Err(e) => return Err(e.into()),
    };

    for entry in direntry {
        match entry.file_type() {
            Ok(entry_type) => {
                if entry_type.is_file() {
                    println!(
                        "File {:?} è codice rust? {:?}",
                        entry.path(),
                        is_rust_source_file(&entry)?
                    )
                } else if entry_type.is_dir() {
                    return show_valid_rust_file(&entry);
                }
            }
            Err(e) => return Err(e.into()),
        }
    }

    return Ok(());
}

fn get_list_entry(dir: &fs::DirEntry) -> Result<Vec<fs::DirEntry>, Box<dyn error::Error>> {
    let mut dirs = vec![];

    let dir_iterator: fs::ReadDir = match fs::read_dir(dir.path()) {
        Ok(a) => a,
        Err(e) => return Result::Err(Box::new(e)),
    };

    for entry in dir_iterator {
        match entry {
            Ok(entry) => dirs.push(entry),
            Err(e) => println!("Errore!\n{}", e),
        }
    }

    return Ok(dirs);
}

fn is_rust_source_file(entry: &fs::DirEntry) -> Result<bool, io::Error> {
    let file_type = entry.file_type()?;

    if file_type.is_file() {
        match entry.path().extension() {
            Some(ext) => {
                if ext == "rs" {
                    return Ok(true);
                }
            }
            None => return Ok(false),
        }
    }

    Ok(false)
}

fn run_app() -> Result<(), Box<dyn error::Error>> {
    let root_path = path::Path::new("/etc");

    // versione corta
    let dir_iterator: fs::ReadDir = fs::read_dir(root_path)?;

    for entry in dir_iterator {
        match entry {
            Ok(entry) => {
                match show_valid_rust_file(&entry) {
                    Ok(()) => (),
                    Err(e) => println!("Errore lettura file {:?}\t{:?}", entry.path(), e),
                };
            }
            Err(e) => println!("Errore!\n{}", e),
        }
    }

    Ok(())
}

fn main() {
    std::process::exit(match run_app() {
        Ok(_) => 0,
        Err(err) => {
            eprintln!("error: {:?}", err);
            1
        }
    });
}
